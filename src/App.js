import React, { Fragment, useState, useEffect } from "react";
import axios from "axios"; // Responsável por pegar params para fazer requisições de uma página na web
import dotenv from "dotenv"; // Responsável por gerênciar variáveis no ambiente de projeto fazendo com que não faz a exposição de suas keys de api e outras chaves importantes

function App() {
  const [location, setLocation] = useState(false); // Estado onde armazena se a localização foi permitida
  const [weather, setWeather] = useState(false); // Estado onde armazena a resposta da API

  dotenv.config(); // Configura para habilitar instâncias de variáveis .env

  // Esta function assíncrona recebe dois parâmetros a latitude e longitude do cliente, logo após o axios faz a requisição com os parâmetros que temos e sua resposta é armazenada em uma variável res
  let getWeather = async (lat, long) => {
    let res = await axios.get(
      "http://api.openweathermap.org/data/2.5/weather",
      {
        params: {
          lat: lat,
          lon: long,
          appid: process.env.REACT_APP_OPEN_WHEATHER_KEY,
          lang: "pt",
          units: "metric",
        },
      }
    );
    setWeather(res.data); // Setamos a resposta da API na nossa state weather
    console.log(res.data); // Exibição de log para ajudar no processo de desenvolvimento
  };

  // Hook que auxilia em toda vez que a página é carregada pela primeira vez as primeiras funções executas são a do useEffect
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      // Function que pega a posição de acordo com a localização do BROWSER
      getWeather(position.coords.latitude, position.coords.longitude); //Chamada na função getWeather passsando os parâmetros de resposta da função acima
      setLocation(true); // Caso a função tenha sido executada e pegamos a localização mudamos nossa state para true. Já que temos os dados necessários
    });
  }, []);

  // Fazemos uma validação caso não tenhamos acesso a localização para dizer para o cliente da permissão caso haver a localização trazemos a resposta dos dados da API de condições climáticas local

  if (location === false) {
    return (
      <Fragment>
        <h3>
          Precisa da permissão e habilitar a localização do seu navegador!
        </h3>
      </Fragment>
    );
  } else {
    return (
      //Exibe do nosso state um objeto as informações que vieram como resposta da API
      <Fragment>
        <h3>
          Clima nas suas coordenadas:({weather["weather"][0]["description"]})
        </h3>
        <hr />
        <ul>
          <li>Temperatura atual:{weather["main"]["temp"]}°</li>
          <li>Temperatura máxima:{weather["main"]["temp_max"]}°</li>
          <li>Temperatura mínima:{weather["main"]["temp_min"]}°</li>
          <li>Pressão:{weather["main"]["pressure"]} hpa</li>
          <li>Umidade:{weather["main"]["humidity"]}%</li>
        </ul>
      </Fragment>
    );
  }
}

export default App;
